<?php

/* /var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/layouts/defautl.htm */
class __TwigTemplate_3e6df47aa335f8bea3af37e5cae06d5fb40f630d3b410bc021c630b6ede740b3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <title> Dự Án Chung Cư UDIC Tây Hồ</title>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

  <base href=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/ ");
        echo "\" >
  <base src=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/ ");
        echo "\" >

  <link rel=\"stylesheet\" href=\"lib/animate/animate.css\">
  <link rel=\"stylesheet\" href=\"lib/bootstrap/bootstrap.min.css\">
  <link rel=\"stylesheet\" href=\"lib/owl-carousel/owl.carousel.min.css\">
  <link rel=\"stylesheet\" href=\"lib/owl-carousel/owl.theme.default.min.css\">
  <link rel=\"stylesheet\" href=\"lib/fancybox/jquery.fancybox.min.css\">
  <link rel=\"stylesheet\" href=\"css/font-awesome.min.css\">
  <link href=\"css/style.css\" rel=\"stylesheet\">

</head>
<body data-spy=\"scroll\" data-target=\".navbar\" data-offset=\"70\">

  <div id=\"loading\" >
      <div id=\"loading-center\">
          <div id=\"loading-center-absolute\">
              <div class=\"object\" id=\"object_one\"></div>
              <div class=\"object\" id=\"object_two\"></div>
              <div class=\"object\" id=\"object_three\"></div>
              <div class=\"object\" id=\"object_four\"></div>
          </div>
      </div>
  </div>

    ";
        // line 33
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 34
        echo "  <!-- /header -->

     ";
        // line 36
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 37
        echo "  <!-- /content -->

    ";
        // line 39
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("contact"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 40
        echo "
    ";
        // line 41
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 42
        echo "  <!-- /footer -->

  <!-- /modal -->
   <div class=\"md-overlay\"></div>


  <script type=\"text/javascript\" src=\"lib/jquery/jquery-3.3.1.min.js\"></script>
  <!-- <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\"></script> -->
  <script type=\"text/javascript\" src=\"lib/bootstrap/bootstrap.min.js\"></script>
  <script type=\"text/javascript\" src=\"lib/bootstrap/bootstrap.bundle.min.js\"></script>
  <script type=\"text/javascript\" src=\"lib/animate/wow.min.js\"></script>
  <script type=\"text/javascript\" src=\"lib/owl-carousel/owl.carousel.min.js\"></script>
  <script type=\"text/javascript\" src=\"lib/fancybox/jquery.fancybox.min.js\"></script>
  <script type=\"text/javascript\" src=\"js/index.js\"></script>

<script type=\"text/javascript\" src=\"https://pixeladd.com/public/plugin/pixelchat/pixelchat.js\"></script><script type=\"text/javascript\">var pixelC= new pixelChat({memCode:\"92dda98d7d86536e7111926b72e7233e\",idPixelChat:\"97\"});pixelC.showChat();</script>

  ";
        // line 59
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 60
        echo "  ";
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 61
        echo "
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/layouts/defautl.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 61,  122 => 60,  107 => 59,  88 => 42,  84 => 41,  81 => 40,  77 => 39,  73 => 37,  71 => 36,  67 => 34,  63 => 33,  36 => 9,  32 => 8,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
  <title> Dự Án Chung Cư UDIC Tây Hồ</title>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

  <base href=\"{{'assets/ '|theme}}\" >
  <base src=\"{{'assets/ '|theme}}\" >

  <link rel=\"stylesheet\" href=\"lib/animate/animate.css\">
  <link rel=\"stylesheet\" href=\"lib/bootstrap/bootstrap.min.css\">
  <link rel=\"stylesheet\" href=\"lib/owl-carousel/owl.carousel.min.css\">
  <link rel=\"stylesheet\" href=\"lib/owl-carousel/owl.theme.default.min.css\">
  <link rel=\"stylesheet\" href=\"lib/fancybox/jquery.fancybox.min.css\">
  <link rel=\"stylesheet\" href=\"css/font-awesome.min.css\">
  <link href=\"css/style.css\" rel=\"stylesheet\">

</head>
<body data-spy=\"scroll\" data-target=\".navbar\" data-offset=\"70\">

  <div id=\"loading\" >
      <div id=\"loading-center\">
          <div id=\"loading-center-absolute\">
              <div class=\"object\" id=\"object_one\"></div>
              <div class=\"object\" id=\"object_two\"></div>
              <div class=\"object\" id=\"object_three\"></div>
              <div class=\"object\" id=\"object_four\"></div>
          </div>
      </div>
  </div>

    {% partial 'header' %}
  <!-- /header -->

     {% page %}
  <!-- /content -->

    {% partial 'contact' %}

    {% partial 'footer' %}
  <!-- /footer -->

  <!-- /modal -->
   <div class=\"md-overlay\"></div>


  <script type=\"text/javascript\" src=\"lib/jquery/jquery-3.3.1.min.js\"></script>
  <!-- <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\"></script> -->
  <script type=\"text/javascript\" src=\"lib/bootstrap/bootstrap.min.js\"></script>
  <script type=\"text/javascript\" src=\"lib/bootstrap/bootstrap.bundle.min.js\"></script>
  <script type=\"text/javascript\" src=\"lib/animate/wow.min.js\"></script>
  <script type=\"text/javascript\" src=\"lib/owl-carousel/owl.carousel.min.js\"></script>
  <script type=\"text/javascript\" src=\"lib/fancybox/jquery.fancybox.min.js\"></script>
  <script type=\"text/javascript\" src=\"js/index.js\"></script>

<script type=\"text/javascript\" src=\"https://pixeladd.com/public/plugin/pixelchat/pixelchat.js\"></script><script type=\"text/javascript\">var pixelC= new pixelChat({memCode:\"92dda98d7d86536e7111926b72e7233e\",idPixelChat:\"97\"});pixelC.showChat();</script>

  {% framework extras %}
  {% scripts %}

</body>
</html>", "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/layouts/defautl.htm", "");
    }
}
