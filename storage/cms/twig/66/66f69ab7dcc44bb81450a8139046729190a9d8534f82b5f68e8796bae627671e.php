<?php

/* /var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/header.htm */
class __TwigTemplate_4fde06eb3be545007267ff282cc263b001bf03e31c43130aafb82221420ba13f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"header\">
  <div class=\"wrap_menu\">
    <div class=\"container\">
      <nav class=\"navbar\">
        <ul class=\"nav\">
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section1\">Giới thiệu</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section2\">Vị trí</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section3\">Thiết kế</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section4\">Tiện ích</a>
            </li>

            <div class=\"logo\">
            <a href=\"/\"><img src=\"images/logo1.png\" alt=\"\" class=\"img-fluid\"></a>
          </div>

            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section5\">Video</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section6\">Mặt bằng</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section7\">Tin tức</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section8\">Liên hệ</a>
            </li>
          <!--   <li class=\"nav-item custom_1\">
              <a class=\"nav-link\" href=\"#section3\">nhận thông tin dự án</a>
            </li> -->
        </ul>
      </nav>
    </div> <!-- /container -->
  </div> <!-- /wrap_menu -->

  <div class=\"wrap_menu_mobi\">
    <div class=\"container\">
      <!-- button menumobile -->
        <div class=\"open-nav\" onclick=\"openNav()\">
                      <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>
                  </div>

                  <div class=\"logo\">
          <a href=\"/\"><img src=\"images/logo1.png\" alt=\"\" class=\"img-fluid\"></a>
        </div>
                  <!-- button menumobile -->
      <!--menu mobile -->
          <div id=\"menu-mobile\" class=\"hidden-md hidden-lg\">
              <div class=\"nav_mobile\">
                <div class=\"top\">
                   <a href=\"/\"><img src=\"images/logo1.png\" alt=\"logo\" class=\"img-fluid\"></a>
                     <i class=\"fa fa-times click_bg\"  aria-hidden=\"true\" onclick=\"closeNav()\"></i>
                </div>

                  <ul class=\"nav\">
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section1\" onclick=\"closeNav()\">giới thiệu</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section2\" onclick=\"closeNav()\">vị trí</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section3\" onclick=\"closeNav()\">thiết kế</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section4\" onclick=\"closeNav()\"> tiện ích</a>
                </li>

                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section5\" onclick=\"closeNav()\">video</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section6\" onclick=\"closeNav()\"> mặt bằng</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section7\" onclick=\"closeNav()\">tin tức</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section8\" onclick=\"closeNav()\">liên hệ</a>
                </li>
              <!--   <li class=\"nav-item custom_1\">
                  <a class=\"nav-link\" href=\"#section3\">nhận thông tin dự án</a>
                </li> -->
            </ul>


              </div>
          </div>
          <div class=\"click_out\"></div>
      <!--end menu mobile -->
    </div> <!-- /container -->
  </div> <!-- /wrap_menu_mobi -->

  <div class=\"wrap_banner\">
    <div class=\"container\">
      <div class=\"info_banner\">
        <div class=\"layout_text\">
          <h3 class=\"wow fadeInLeft\" data-wow-delay=\"0.7s\" data-wow-duration=\"1.2s\">Udic westlake tây hồ</h3>
          <h2 class=\"wow fadeInLeft\" data-wow-delay=\"0.8s\" data-wow-duration=\"1.2s\">Đẳng cấp căn hộ đáng sống</h2>
        </div> <!-- /layout_text -->

        <div class=\"layout_table wow fadeInRight\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
          <h3>Udic westlake tây hồ</h3>
          <h6>CHUNG CƯ CĂN HỘ CAO CÂP</h6>
          <ul>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Vị trí vàng đường võ chí công</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Hệ thống cơ sở hạ tầng đồng bộ</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Sự đa dạng về tiện ích</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Tiện ích đầy đủ, đồng bộ</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Hệ thống cấp nước sạch đảm bảo</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Năng lượng tiêu dùng xanh, sạch</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Pháp lý minh bạch</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Giá bán hợp lý, nhiều ưu đãi</p></li>
          </ul>
        </div> <!-- /layout_table -->

        <div class=\"layout_mouse\">
          <span class=\"click_down\">
            <i class=\"fa fa-angle-double-down\" aria-hidden=\"true\"></i>
          </span>
        </div> <!-- /layout_mouse -->
      </div> <!-- /info_banner -->
    </div> <!-- /container -->
  </div> <!-- /wrap_banner -->
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/header.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"header\">
  <div class=\"wrap_menu\">
    <div class=\"container\">
      <nav class=\"navbar\">
        <ul class=\"nav\">
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section1\">Giới thiệu</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section2\">Vị trí</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section3\">Thiết kế</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section4\">Tiện ích</a>
            </li>

            <div class=\"logo\">
            <a href=\"/\"><img src=\"images/logo1.png\" alt=\"\" class=\"img-fluid\"></a>
          </div>

            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section5\">Video</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section6\">Mặt bằng</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section7\">Tin tức</a>
            </li>
            <li class=\"nav-item\">
              <a class=\"nav-link\" href=\"#section8\">Liên hệ</a>
            </li>
          <!--   <li class=\"nav-item custom_1\">
              <a class=\"nav-link\" href=\"#section3\">nhận thông tin dự án</a>
            </li> -->
        </ul>
      </nav>
    </div> <!-- /container -->
  </div> <!-- /wrap_menu -->

  <div class=\"wrap_menu_mobi\">
    <div class=\"container\">
      <!-- button menumobile -->
        <div class=\"open-nav\" onclick=\"openNav()\">
                      <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>
                  </div>

                  <div class=\"logo\">
          <a href=\"/\"><img src=\"images/logo1.png\" alt=\"\" class=\"img-fluid\"></a>
        </div>
                  <!-- button menumobile -->
      <!--menu mobile -->
          <div id=\"menu-mobile\" class=\"hidden-md hidden-lg\">
              <div class=\"nav_mobile\">
                <div class=\"top\">
                   <a href=\"/\"><img src=\"images/logo1.png\" alt=\"logo\" class=\"img-fluid\"></a>
                     <i class=\"fa fa-times click_bg\"  aria-hidden=\"true\" onclick=\"closeNav()\"></i>
                </div>

                  <ul class=\"nav\">
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section1\" onclick=\"closeNav()\">giới thiệu</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section2\" onclick=\"closeNav()\">vị trí</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section3\" onclick=\"closeNav()\">thiết kế</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section4\" onclick=\"closeNav()\"> tiện ích</a>
                </li>

                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section5\" onclick=\"closeNav()\">video</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section6\" onclick=\"closeNav()\"> mặt bằng</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section7\" onclick=\"closeNav()\">tin tức</a>
                </li>
                <li class=\"nav-item\">
                  <a class=\"nav-link\" href=\"https://udictayho.com/#section8\" onclick=\"closeNav()\">liên hệ</a>
                </li>
              <!--   <li class=\"nav-item custom_1\">
                  <a class=\"nav-link\" href=\"#section3\">nhận thông tin dự án</a>
                </li> -->
            </ul>


              </div>
          </div>
          <div class=\"click_out\"></div>
      <!--end menu mobile -->
    </div> <!-- /container -->
  </div> <!-- /wrap_menu_mobi -->

  <div class=\"wrap_banner\">
    <div class=\"container\">
      <div class=\"info_banner\">
        <div class=\"layout_text\">
          <h3 class=\"wow fadeInLeft\" data-wow-delay=\"0.7s\" data-wow-duration=\"1.2s\">Udic westlake tây hồ</h3>
          <h2 class=\"wow fadeInLeft\" data-wow-delay=\"0.8s\" data-wow-duration=\"1.2s\">Đẳng cấp căn hộ đáng sống</h2>
        </div> <!-- /layout_text -->

        <div class=\"layout_table wow fadeInRight\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
          <h3>Udic westlake tây hồ</h3>
          <h6>CHUNG CƯ CĂN HỘ CAO CÂP</h6>
          <ul>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Vị trí vàng đường võ chí công</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Hệ thống cơ sở hạ tầng đồng bộ</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Sự đa dạng về tiện ích</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Tiện ích đầy đủ, đồng bộ</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Hệ thống cấp nước sạch đảm bảo</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Năng lượng tiêu dùng xanh, sạch</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Pháp lý minh bạch</p></li>
            <li><p><i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i> Giá bán hợp lý, nhiều ưu đãi</p></li>
          </ul>
        </div> <!-- /layout_table -->

        <div class=\"layout_mouse\">
          <span class=\"click_down\">
            <i class=\"fa fa-angle-double-down\" aria-hidden=\"true\"></i>
          </span>
        </div> <!-- /layout_mouse -->
      </div> <!-- /info_banner -->
    </div> <!-- /container -->
  </div> <!-- /wrap_banner -->
</div>", "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/header.htm", "");
    }
}
