<?php namespace Thienvietjsc\Web\Components;
use Cms\Classes\ComponentBase;
use Thienvietjsc\Web\Models\Content as ContentM;

class Content extends ComponentBase{
 	public function componentDetails(){
		return [
			'name' => 'Content',
			'description' => 'Nội dung website'
		];
	}
	public function onRun()
	{
		$this->page['content'] = ContentM::first();
	}

  
 }

