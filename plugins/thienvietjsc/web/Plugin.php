<?php namespace Thienvietjsc\Web;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
          'Thienvietjsc\Web\Components\Contact'         => 'contact',
          'Thienvietjsc\Web\Components\General'         => 'General',
          'Thienvietjsc\Web\Components\Content'         => 'Content',
          'Thienvietjsc\Web\Components\ShopHouse'         => 'ShopHouse',
          'Thienvietjsc\Web\Components\Condotel'         => 'Condotel',
          'Thienvietjsc\Web\Components\About'         => 'About',
        ];
    }

    public function registerSettings()
    {
    }
}
