<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateThienvietjscWebContact extends Migration
{
    public function up()
    {
        Schema::create('thienvietjsc_web_contact', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 200);
            $table->string('phone', 20);
            $table->string('email', 60);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('thienvietjsc_web_contact');
    }
}
