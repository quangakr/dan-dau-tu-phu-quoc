<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateThienvietjscWebSite extends Migration
{
    public function up()
    {
        Schema::create('thienvietjsc_web_site', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('name', 500);
            $table->string('description', 2000);
            $table->string('email', 200)->nullable();
            $table->string('phone', 100)->nullable();
            $table->string('link_fb', 500)->nullable();
            $table->string('link_gg', 500)->nullable();
            $table->string('link_yt', 500)->nullable();
            $table->string('location', 500)->nullable();
            $table->string('company', 1000)->nullable();
            $table->string('iframe_fb', 500)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('thienvietjsc_web_site');
    }
}
