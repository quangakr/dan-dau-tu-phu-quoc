<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateThienvietjscWebSite extends Migration
{
    public function up()
    {
        Schema::table('thienvietjsc_web_site', function($table)
        {
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('thienvietjsc_web_site', function($table)
        {
            $table->dropColumn('id');
        });
    }
}
