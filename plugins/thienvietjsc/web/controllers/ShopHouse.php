<?php namespace Thienvietjsc\Web\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class ShopHouse extends Controller
{
    public $implement = [        'Backend\Behaviors\FormController'    ];
    
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Thienvietjsc.Web', 'main-menu-item2', 'side-menu-item3');
    }
}
