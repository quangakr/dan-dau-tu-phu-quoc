<?php namespace Thienvietjsc\Web\Components;
use Cms\Classes\ComponentBase;
use Thienvietjsc\Web\Models\Condotel as Condotels;

class Condotel extends ComponentBase{
 	public function componentDetails(){
		return [
			'name' => 'Condotel',
			'description' => 'Thông tin condotel'
		];
	}
	public function onRun()
	{
		$this->page['condotel'] = Condotels::first();
	}

  
 }

