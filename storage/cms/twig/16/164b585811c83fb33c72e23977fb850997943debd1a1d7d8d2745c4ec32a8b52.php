<?php

/* /var/www/vhosts/udictayho.com/httpdocs/plugins/rainlab/blog/components/post/default.htm */
class __TwigTemplate_46e448442bfabdbe670a2f3991748ee0c452301594ac6bf090658111679513bf extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $context["post"] = twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "post", array());
        // line 3
        echo "

<div class=\"content\">
    <div class=\"container\">
        <div class=\"wrap_main_content\">
            <h2>";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", array()), "html", null, true);
        echo "</h2>

                ";
        // line 10
        echo twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "content_html", array());
        echo "

                 <div class=\"author_post\">
                    <p>Tác giả: <span>admin</span></p>
                    <p>
                        chia sẻ: <a href=\"/\"><i class=\"fa fa-facebook-official\" aria-hidden=\"true\"></i></a>
                                <a href=\"/\"><i class=\"fa fa-google-plus-square\" aria-hidden=\"true\"></i></a>
                    </p>
                 </div> <!-- /author_post -->
        </div> <!-- /wrap_main_content -->



    </div> <!-- /container -->
</div> <!-- /content -->
";
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/plugins/rainlab/blog/components/post/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 10,  35 => 8,  28 => 3,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% set post = __SELF__.post %}


<div class=\"content\">
    <div class=\"container\">
        <div class=\"wrap_main_content\">
            <h2>{{post.title}}</h2>

                {{ post.content_html|raw }}

                 <div class=\"author_post\">
                    <p>Tác giả: <span>admin</span></p>
                    <p>
                        chia sẻ: <a href=\"/\"><i class=\"fa fa-facebook-official\" aria-hidden=\"true\"></i></a>
                                <a href=\"/\"><i class=\"fa fa-google-plus-square\" aria-hidden=\"true\"></i></a>
                    </p>
                 </div> <!-- /author_post -->
        </div> <!-- /wrap_main_content -->



    </div> <!-- /container -->
</div> <!-- /content -->
", "/var/www/vhosts/udictayho.com/httpdocs/plugins/rainlab/blog/components/post/default.htm", "");
    }
}
