<?php namespace Thienvietjsc\Web\Components;
use Cms\Classes\ComponentBase;
use Thienvietjsc\Web\Models\About as Abouts;

class About extends ComponentBase{
 	public function componentDetails(){
		return [
			'name' => 'About',
			'description' => 'Thông tin trang giới thiệu'
		];
	}
	public function onRun()
	{
		$this->page['about'] = Abouts::first();
	}

  
 }
