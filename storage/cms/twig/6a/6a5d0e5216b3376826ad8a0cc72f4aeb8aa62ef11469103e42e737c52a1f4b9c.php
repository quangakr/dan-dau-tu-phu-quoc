<?php

/* /var/www/vhosts/udictayho.com/httpdocs/plugins/rainlab/blog/components/posts/default.htm */
class __TwigTemplate_5dac48587ff01aa1b8aa555f9e6333ad94b9f3900227029494e4615ccbbd3fed extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "posts", array());
        // line 2
        echo "
<div class=\"owl-carousel owl-theme custom_owl_2\">
";
        // line 4
        if (($context["posts"] ?? null)) {
            // line 5
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                // line 6
                echo "    ";
                if (twig_get_attribute($this->env, $this->source, $context["post"], "published", array())) {
                    // line 7
                    echo "    <div class=\"item\">
      <div class=\"wrap_content_blog wow slideInUp\" data-wow-delay=\"1s\" data-wow-duration=\"1s\">
        <div class=\"images\">
          <a href=\"";
                    // line 10
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", array()), "html", null, true);
                    echo "\"><img src=\"";
                    echo call_user_func_array($this->env->getFilter('resize')->getCallable(), array(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", array()), "path", array()), 362, 203, array("mode" => "crop", "extension" => "jpg")));
                    echo "\" alt=\"\" class=\"img-fluid\"></a>
        </div>
        <div class=\"caption\">
          <span class=\"date_submit\">
            Ngày đăng:   ";
                    // line 14
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "published_at", array()), "d/m/Y"), "html", null, true);
                    echo "
          </span>
          <h4>";
                    // line 16
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "title", array()), "html", null, true);
                    echo "</h4>

          <p >
            ";
                    // line 19
                    echo twig_get_attribute($this->env, $this->source, $context["post"], "summary", array());
                    echo "
          </p>

          <a href=\"";
                    // line 22
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "url", array()), "html", null, true);
                    echo "\" class=\"read_more\"> Chi tiết <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i></a>
        </div>
      </div> <!-- /wrap_content_blog -->
    </div> <!-- /item -->
    ";
                }
                // line 27
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 30
            echo "    <div class=\"item\">
      <div class=\"wrap_content_blog wow slideInUp\" data-wow-delay=\"1s\" data-wow-duration=\"1s\">
        <h4>Đang nhập nhật</h4>
      </div> <!-- /wrap_content_blog -->
    </div> <!-- /item -->
";
        }
        // line 36
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/plugins/rainlab/blog/components/posts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 36,  85 => 30,  77 => 27,  69 => 22,  63 => 19,  57 => 16,  52 => 14,  43 => 10,  38 => 7,  35 => 6,  31 => 5,  29 => 4,  25 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set posts = __SELF__.posts %}

<div class=\"owl-carousel owl-theme custom_owl_2\">
{% if posts %}
{% for post in posts %}
    {% if post.published %}
    <div class=\"item\">
      <div class=\"wrap_content_blog wow slideInUp\" data-wow-delay=\"1s\" data-wow-duration=\"1s\">
        <div class=\"images\">
          <a href=\"{{ post.url }}\"><img src=\"{{ post.featured_images.path|resize(362,203, { mode: 'crop', extension: 'jpg' }) }}\" alt=\"\" class=\"img-fluid\"></a>
        </div>
        <div class=\"caption\">
          <span class=\"date_submit\">
            Ngày đăng:   {{ post.published_at|date('d/m/Y') }}
          </span>
          <h4>{{ post.title }}</h4>

          <p >
            {{ post.summary|raw }}
          </p>

          <a href=\"{{ post.url }}\" class=\"read_more\"> Chi tiết <i class=\"fa fa-arrow-right\" aria-hidden=\"true\"></i></a>
        </div>
      </div> <!-- /wrap_content_blog -->
    </div> <!-- /item -->
    {%endif%}

{% endfor %}
{% else %}
    <div class=\"item\">
      <div class=\"wrap_content_blog wow slideInUp\" data-wow-delay=\"1s\" data-wow-duration=\"1s\">
        <h4>Đang nhập nhật</h4>
      </div> <!-- /wrap_content_blog -->
    </div> <!-- /item -->
{%endif%}
</div>
", "/var/www/vhosts/udictayho.com/httpdocs/plugins/rainlab/blog/components/posts/default.htm", "");
    }
}
