<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateThienvietjscWebContent extends Migration
{
    public function up()
    {
        Schema::create('thienvietjsc_web_content', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 500);
            $table->text('content1')->nullable();
            $table->text('content2')->nullable();
            $table->text('content3')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('thienvietjsc_web_content');
    }
}
