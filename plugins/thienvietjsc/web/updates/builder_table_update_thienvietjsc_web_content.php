<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateThienvietjscWebContent extends Migration
{
    public function up()
    {
        Schema::table('thienvietjsc_web_content', function($table)
        {
            $table->string('namec2', 500);
            $table->string('namec3', 500);
            $table->renameColumn('name', 'namec1');
        });
    }
    
    public function down()
    {
        Schema::table('thienvietjsc_web_content', function($table)
        {
            $table->dropColumn('namec2');
            $table->dropColumn('namec3');
            $table->renameColumn('namec1', 'name');
        });
    }
}
