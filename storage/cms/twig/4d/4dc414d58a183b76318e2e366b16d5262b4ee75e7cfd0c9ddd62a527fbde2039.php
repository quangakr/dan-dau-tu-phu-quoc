<?php

/* /var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/footer.htm */
class __TwigTemplate_ba2ecdc81e9478b6290cecaa28e828d1d32352a8602547ac32f89c3ab81c5619 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"footer\">
  <div class=\"container\">
    <div class=\"phone_center\">
          <a href=\"tel:0948699668\" id=\"alo-phoneIcon\"  class=\"alo-phone alo-green alo-show\">
              <div class=\"alo-ph-circle\"></div>
              <div class=\"alo-ph-circle-fill\"></div>
              <div class=\"alo-ph-img-circle\"><i class=\"fa fa-phone\"></i></div>
              <span class=\"alo-ph-text\">0948699668</span>
          </a>
          <a href=\"tel:0904565286\" id=\"alo-phoneIcon\" class=\"alo-phone alo-green alo-show alophone_fix\">
              <span class=\"alo-ph-text\">0904565286</span>
          </a>
          <!-- <a href=\"tel:0948699668\" ><i class=\"fa fa-phone\"></i>0948699668</a> -->
      </div> <!-- /phone_center -->


    <div class='scrolltop'>
      <a href=\"#\" class=\"go-top\"><i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i></a>
    </div> <!-- /scrolltop -->
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/footer.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"footer\">
  <div class=\"container\">
    <div class=\"phone_center\">
          <a href=\"tel:0948699668\" id=\"alo-phoneIcon\"  class=\"alo-phone alo-green alo-show\">
              <div class=\"alo-ph-circle\"></div>
              <div class=\"alo-ph-circle-fill\"></div>
              <div class=\"alo-ph-img-circle\"><i class=\"fa fa-phone\"></i></div>
              <span class=\"alo-ph-text\">0948699668</span>
          </a>
          <a href=\"tel:0904565286\" id=\"alo-phoneIcon\" class=\"alo-phone alo-green alo-show alophone_fix\">
              <span class=\"alo-ph-text\">0904565286</span>
          </a>
          <!-- <a href=\"tel:0948699668\" ><i class=\"fa fa-phone\"></i>0948699668</a> -->
      </div> <!-- /phone_center -->


    <div class='scrolltop'>
      <a href=\"#\" class=\"go-top\"><i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i></a>
    </div> <!-- /scrolltop -->
  </div>
</div>", "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/footer.htm", "");
    }
}
