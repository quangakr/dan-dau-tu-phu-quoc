<?php

/* /var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/contact.htm */
class __TwigTemplate_8d31ac39830fb26a2e96416d500474910b9a0893b7c88d03e0eaeb1af59b9a92 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"wrap_section8\" id=\"section8\">
    <div class=\"container\">
      <div class=\"main_info\">
        <div class=\"row\">
          <div class=\"col-lg-5\">
            <div class=\"contact_info wow fadeInLeft\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
              <h2>liên hệ</h2>
              <p>
                Sàn giao dịch Bất động sản Đại Phú Mỹ Hưng
              </p>
              <div class=\"logo\">
                <a href=\"/\"><img src=\"images/logo.png\" alt=\"\" class=\"img-fluid\"></a>
              </div>
              <div class=\"contact\">
                <ul>
                  <li><p><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i> <span> Tầng I + 2, Số 20, Phố Ngụy Như Kon Tum, Phường Nhân Chính, Quận Thanh Xuân, Hà Nội</span></p></li>
                  <li>
                    <p><i class=\"fa fa-phone\" aria-hidden=\"true\"></i>  <span>0948699668 &  0904565286</span></p>
                  </li>
                  <li>
                    <p><i class=\"fa fa-globe\" aria-hidden=\"true\"></i> <span style=\"text-transform: lowercase;\">namnt@daiphumyhung.com</span></p>
                  </li>
                </ul>
              </div>
              <div class=\"copy_right\">
                <p>
                  Copyright © 2018 Dai Phu My Hung Group
                </p>
              </div>
            </div>
          </div> <!-- /col -->

          <div class=\"col-lg-7\">
            <div class=\"form_contact wow fadeInRight\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
              <h2>đăng ký <br> nhận thông tin dự án</h2>
              ";
        // line 36
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("contact"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 37
        echo "            </div> <!-- /form_contact -->
          </div> <!-- /col -->
        </div>
      </div><!--  /main_info -->
    </div> <!-- /container -->
  </div>";
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 37,  60 => 36,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"wrap_section8\" id=\"section8\">
    <div class=\"container\">
      <div class=\"main_info\">
        <div class=\"row\">
          <div class=\"col-lg-5\">
            <div class=\"contact_info wow fadeInLeft\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
              <h2>liên hệ</h2>
              <p>
                Sàn giao dịch Bất động sản Đại Phú Mỹ Hưng
              </p>
              <div class=\"logo\">
                <a href=\"/\"><img src=\"images/logo.png\" alt=\"\" class=\"img-fluid\"></a>
              </div>
              <div class=\"contact\">
                <ul>
                  <li><p><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i> <span> Tầng I + 2, Số 20, Phố Ngụy Như Kon Tum, Phường Nhân Chính, Quận Thanh Xuân, Hà Nội</span></p></li>
                  <li>
                    <p><i class=\"fa fa-phone\" aria-hidden=\"true\"></i>  <span>0948699668 &  0904565286</span></p>
                  </li>
                  <li>
                    <p><i class=\"fa fa-globe\" aria-hidden=\"true\"></i> <span style=\"text-transform: lowercase;\">namnt@daiphumyhung.com</span></p>
                  </li>
                </ul>
              </div>
              <div class=\"copy_right\">
                <p>
                  Copyright © 2018 Dai Phu My Hung Group
                </p>
              </div>
            </div>
          </div> <!-- /col -->

          <div class=\"col-lg-7\">
            <div class=\"form_contact wow fadeInRight\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
              <h2>đăng ký <br> nhận thông tin dự án</h2>
              {% component 'contact' %}
            </div> <!-- /form_contact -->
          </div> <!-- /col -->
        </div>
      </div><!--  /main_info -->
    </div> <!-- /container -->
  </div>", "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/contact.htm", "");
    }
}
