<?php

/* /var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/modal.htm */
class __TwigTemplate_119837558c348e1a75563882a253467b3c62b2e421e92aaaf59a28242376b578 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"md-modal\">
  <div class=\"md-content\">
    <div class=\"info_popup\">
      <div class=\"row\">
        <div class=\"col-lg-6 col-md-6\">
          <div class=\"indext wow0\">
            <h3>đăng ký ngay</h3>
            <p>Nhận bộ tài liệu</p>
            <ul>
              <li>Thông tin đầy đủ dự án</li>
              <li>Mặt bằng khu</li>
              <li>Bảng giá chi tiết từng căn</li>
              <li>Tiến độ thanh toán</li>
              <li>Thư mời tham quán miễn phí</li>
              <li>Chương trình ưu đãi mới nhất</li>
              <li>Cam kết cho thuê lại</li>
            </ul>
          </div>
        </div> <!-- /col -->
        <div class=\"col-lg-6 col-md-6\">
          <div class=\"form_contact\">
            <h3>thông tin đăng ký</h3>
            ";
        // line 23
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("contact"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 24
        echo "          </div>
        </div> <!-- /col -->
      </div> <!-- /row -->
      <button class=\"md-close\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></button>
    </div> <!-- /info_popup -->
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/modal.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 24,  47 => 23,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"md-modal\">
  <div class=\"md-content\">
    <div class=\"info_popup\">
      <div class=\"row\">
        <div class=\"col-lg-6 col-md-6\">
          <div class=\"indext wow0\">
            <h3>đăng ký ngay</h3>
            <p>Nhận bộ tài liệu</p>
            <ul>
              <li>Thông tin đầy đủ dự án</li>
              <li>Mặt bằng khu</li>
              <li>Bảng giá chi tiết từng căn</li>
              <li>Tiến độ thanh toán</li>
              <li>Thư mời tham quán miễn phí</li>
              <li>Chương trình ưu đãi mới nhất</li>
              <li>Cam kết cho thuê lại</li>
            </ul>
          </div>
        </div> <!-- /col -->
        <div class=\"col-lg-6 col-md-6\">
          <div class=\"form_contact\">
            <h3>thông tin đăng ký</h3>
            {% component 'contact' %}
          </div>
        </div> <!-- /col -->
      </div> <!-- /row -->
      <button class=\"md-close\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></button>
    </div> <!-- /info_popup -->
  </div>
</div>", "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/partials/modal.htm", "");
    }
}
