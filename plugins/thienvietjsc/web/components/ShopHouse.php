<?php namespace Thienvietjsc\Web\Components;
use Cms\Classes\ComponentBase;
use Thienvietjsc\Web\Models\ShopHouse as ShopHouses;

class ShopHouse extends ComponentBase{
 	public function componentDetails(){
		return [
			'name' => 'ShopHouse',
			'description' => 'Thông tin shophouse'
		];
	}
	public function onRun()
	{
		$this->page['shophouse'] = ShopHouses::first();
	}

  
 }

