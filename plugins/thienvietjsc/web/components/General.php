<?php namespace Thienvietjsc\Web\Components;
use Cms\Classes\ComponentBase;
use Thienvietjsc\Web\Models\GeneralSite;

class General extends ComponentBase{
 	public function componentDetails(){
		return [
			'name' => 'Genaral',
			'description' => 'Thông tin chung'
		];
	}
	public function onRun()
	{
		$this->page['general'] = GeneralSite::first();
	}

  
 }

