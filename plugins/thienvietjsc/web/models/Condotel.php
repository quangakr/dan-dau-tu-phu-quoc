<?php namespace Thienvietjsc\Web\Models;

use Model;

/**
 * Model
 */
class Condotel extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'thienvietjsc_web_condotel';

    protected $jsonable = ['contentc1','contentc2','contentc3','contentc4','contentc5'];
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachMany = [
        'images'   => 'System\Models\File',
    ];
}
