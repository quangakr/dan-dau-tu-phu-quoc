<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateThienvietjscWebAbout extends Migration
{
    public function up()
    {
        Schema::create('thienvietjsc_web_about', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('image', 500);
            $table->string('description', 2000);
            $table->text('content');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('thienvietjsc_web_about');
    }
}
