<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateThienvietjscWebContent2 extends Migration
{
    public function up()
    {
        Schema::table('thienvietjsc_web_content', function($table)
        {
            $table->string('description2', 2000)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('thienvietjsc_web_content', function($table)
        {
            $table->dropColumn('description2');
        });
    }
}
