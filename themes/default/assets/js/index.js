$(document).ready(function() {
	new WOW().init();

	// $('.load_more').click(function(event) {
	// 	$(this).parent().toggleClass('active');
		
	// });


	// $('.load_more').click(function(event) {
	// 	$(this).parent().toggleClass('active');
	// 		$(this).toggleClass('active');

	// });

	$(document).ready(function() {

		$('.load_more').click(function(event) {
			$(this).prev().toggleClass('active');
			event.stopPropagation();
			$(this).toggleClass('active');
		});

		$('.dot_content').click(function(event){
			 event.stopPropagation();
		});

		$('body').click(function(){
            if($('.dot_content ,.load_more').hasClass('active')){
                $('.dot_content,.load_more').removeClass('active');
            }
        });
        $('.mainslide').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            fade: false,
            autoplay: true,
        });
        $('.lst_image_project').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            speed: 1000,
            dots: false,
            fade: false,
            autoplay: true,
        });
        $('.lst_dsnews_duc').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            fade: false,
            autoplay: true,
            responsive: [
              {
                  breakpoint: 1199,
                  settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1
                  }
              },
              {
                  breakpoint: 767,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1
                  }
              }
            ]

        });
    });
	$('[data-fancybox="images-preview"]').fancybox({
		  buttons : [ 
		    'slideShow',
		    'zoom',
		    'fullScreen',
		    'close'
		  ],
		  thumbs : {
		    autoStart : true
		  },
		 transitionEffect: "circular",
	});


	$('.custom_owl_1').owlCarousel({
	    loop:true,
	    slideSpeed: 3000,
	    margin:30,
	    navText: ['<i class="fa fa-angle-double-left" aria-hidden="true"></i>','<i class="fa fa-angle-double-right" aria-hidden="true"></i>'],
	    autoplay: true,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:3
	        }
	    }
	});
});

$(document).ready(function() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
        } else {
            $('.go-top').fadeOut(200);
        }
    });
    
    $('.go-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 1000);
    });

  	$(function(){

		 $(window).scroll(function() {
		 	imhere = $('html,body').scrollTop();

		 	if(imhere >44)
		 	{
		 		$('.wrap_bot_header').addClass('active');
		 	}
		 	else if(imhere <44)
		 	{
		 		$('.wrap_bot_header').removeClass('active');
		 	}
		 	
		 })
	});

});


 // menu mobile
var test = 0;

function openNav() {
    if (test == 0) {
        $('#menu-mobile').css({
            transform: 'translateX(0)'
        });
        $('#menu-mobile').css({
            transition: 'all 0.5s'
        });
        $('body').css({
            overflow: 'hidden'
        });
        $('.click_out').css({
            height: '100%',
            width: '100%',
        });
        test = 1;
    } else {
        // console.log(test);
        $('#menu-mobile').css({
            transform: 'translateX(-300px)'
        });
        $('#menu-mobile').css({
            transition: 'all 0.5s'
        });
        $('body').css({
            overflow: 'visible'
        });
        test = 0;
    }
}

function closeNav() {
    $('#menu-mobile').css({
        transform: 'translateX(-300px)'
    });
    $('#menu-mobile').css({
        transition: 'all 1s'
    });
    $('body').css({
        overflow: 'visible'
    });
    $('.click_out').css({
        height: '0%',
        width: '0%',
    });

    test = 0;
}



$('.click_out').on('click', function() {
    // console.log('2');
    $('#menu-mobile').css({
        transform: 'translateX(-300px)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'visible'
    });
    $('.click_out').css({
        height: '0%',
        width: '0%',
        visibility: 'visible',
    });
    test = 0;
});





$(document).ready(function() {
    var ccc = $('#menu-mobile ul li').find('ul');
    if (ccc.length !== 0) {
        ccc.before('<button class="accordion"></button>');
        ccc.addClass('sub-menu');
    }
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "200px";
            }
        }
    }
});


$(document).ready(function () {
    $('.mainslide').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        fade: false,
        autoplay: true,
    });
    $('.lst_image_project').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        speed: 1000,
        dots: false,
        fade: false,
        autoplay: true,
    });
    $('.lst_dsnews_duc').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        fade: false,
        autoplay: true,
        responsive: [
          {
              breakpoint: 1199,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
              }
          },
          {
              breakpoint: 767,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
        ]

    });
});

$(document).ready(function () {
    // Add smooth scrolling on all links inside the navbar
    $(".navbar ul li a").on('click', function (event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        }  // End if
    });
});

$(document).ready(function () {
    $('.sub_price_policy .sub_policy .btn_seemore_policy').click(function () {
        $(this).parent().toggleClass("active");
    });
});