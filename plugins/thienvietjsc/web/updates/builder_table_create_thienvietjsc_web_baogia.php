<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateThienvietjscWebBaogia extends Migration
{
    public function up()
    {
        Schema::create('thienvietjsc_web_baogia', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('email', 200);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('thienvietjsc_web_baogia');
    }
}
