<?php namespace Thienvietjsc\Web\Components;
use Cms\Classes\ComponentBase;
use Input;
use Mail;
use Validator;
use Redirect;
use Messages;
use ValidationException;
use Flash;
use Db;
class Contact extends ComponentBase{
 	public function componentDetails(){
		return [
			'name' => 'Contact',
			'description' => 'Nhận thông tin khách hàng'
		];
	}
	public function onRun(){

	}

  	public function onSend(){
		$data = post();
		// dump(post());
		// die();
	    $rules = [
	        'name'    => 'required',
	        'email'   => 'required|email',
	        'phone'   => 'required',
	    ];
	    // dump($rules);
	    // die();
	    $validator = Validator::make($data, $rules);

	    if ($validator->fails()) {
	        throw new ValidationException($validator);
	    }else{
			$vars = [
				'name'      => Input::get('name'),
				'email'     => Input::get('email'),
				'phone'     => Input::get('phone'),
			];
			
			
			//send mail
			Mail::send('thienvietjsc.web::mail.message', $vars, function($message) {
				
			   	// $message->to('TRINHTTP64@GMAIL.COM', 'Admin Person');
	   			// $message->to('quangakr97@gmail.com', 'Admin Person');
	   			$message->to('dpmh.mkt@gmail.com', 'Admin Person');
			    $message->subject('[DanDauTuPhuQuoc] Khách Hàng nhận báo giá');

			});
			// //save database
			Db::table('thienvietjsc_web_contact')->insert([
                'name'    => $vars['name'],
                'email'   => $vars['email'],
                'phone'   => $vars['phone'],
            ]);
			 Flash::success('Gửi thành công!!');
		}
		
	}
  	public function onSendContact(){
		$data = post();
	    $rules = [
	        'name'    => 'required',
	        'email'   => 'required|email',
	        'content'   => 'required',
	    ];
	    // dump($rules);
	    // die();
	    $validator = Validator::make($data, $rules);

	    if ($validator->fails()) {
	        throw new ValidationException($validator);
	    }else{
			$vars = [
				'name'      => Input::get('name'),
				'email'     => Input::get('email'),
				'content'     => Input::get('content'),
			];
			
			
			//send mail
			Mail::send('thienvietjsc.web::mail.contact', $vars, function($message) {
				
			   	// $message->to('TRINHTTP64@GMAIL.COM', 'Admin Person');
	   			$message->to('dpmh.mkt@gmail.com', 'Admin Person');
	   			// $message->to('quangakr97@gmail.com', 'Admin Person');
			    $message->subject('[DanDauTuPhuQuoc] Khách Hàng Liên Hệ ');

			});
			// //save database
			Db::table('thienvietjsc_web_contact')->insert([
                'name'    => $vars['name'],
                'email'   => $vars['email'],
                'content'   => $vars['content'],
            ]);
			 Flash::success('Gửi thành công!!');
		}
		
	}
	
		
 }

