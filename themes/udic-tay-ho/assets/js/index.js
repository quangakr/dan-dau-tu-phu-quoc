﻿$(document).ready(function() {
	new WOW().init();


	$('.custom_owl_1').owlCarousel({
	    loop:true,
	    slideSpeed: 3000,
	    margin:0,
	    autoplay: true,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('.custom_owl_2').owlCarousel({
	    loop:true,
	    slideSpeed: 3000,
	    margin:15,
	    autoplay: true,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('.custom_owl_3').owlCarousel({
	    loop:true,
	    slideSpeed: 3000,
	    margin:15,
	    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
	    autoplay: true,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:2
	        }
	    }
	});

	$(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
        } else {
            $('.go-top').fadeOut(200);
        }
    });
    
    $('.go-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 1000);
    });


    $('.fixed_notification').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.form_contact').offset().top}, 1000);
      });
});


// $(document).ready(function(){
//        var ring = setInterval(function() {
//             $('.md-modal').addClass('md-show');
//             clearInterval(ring);
//     },10000);
        
      
//       $('.md-close, .md-overlay').click(function(event) {
//             $('.md-modal').removeClass('md-show');
//         });
// });


$(document).ready(function() {
  $(function(){
     $(window).scroll(function() {
        imhere = $('html,body').scrollTop();
        if(imhere >117)
        {
            $('.wrap_menu , .fixed_sidebar_left ').addClass('active');
        }
        else if(imhere <117)
        {
            $('.wrap_menu , .fixed_sidebar_left ').removeClass('active');
        }
        
     })
    })
});



$(document).ready(function() {
       
   $("nav.navbar ul>li>a").on('click', function(event) {
       if (this.hash !== "") {
             event.preventDefault();
             var hash = this.hash;
             $('html, body').animate({ scrollTop: $(hash).offset().top }, 1000, function(){ 
             	window.location.hash = hash; 
             });
            
        }
    }); 
});


var test = 0;

function openNav() {
    if (test == 0) {
        console.log(test);
        $('#menu-mobile').css({
            transform: 'translateX(0)'
        });
        $('#menu-mobile').css({
            transition: 'all 0.5s'
        });
        $('body').css({
            overflow: 'hidden'
        });
        $('.click_out').css({
            height: '100%',
            width: '100%',
        });
        test = 1;
    } else {
        // console.log(test);
        $('#menu-mobile').css({
            transform: 'translateX(-300px)'
        });
        $('#menu-mobile').css({
            transition: 'all 0.5s'
        });
        $('body').css({
            overflow: 'visible'
        });
        test = 0;
    }
}

function closeNav() {
    $('#menu-mobile').css({
        transform: 'translateX(-300px)'
    });
    $('#menu-mobile').css({
        transition: 'all 1s'
    });
    $('body').css({
        overflow: 'visible'
    });
    $('.click_out').css({
        height: '0%',
        width: '0%',
    });

    test = 0;
}


$('.click_out').on('click', function() {
    // console.log('2');
    $('#menu-mobile').css({
        transform: 'translateX(-300px)'
    });
    $('#menu-mobile').css({
        transition: 'all 0.5s'
    });
    $('body').css({
        overflow: 'visible'
    });
    $('.click_out').css({
        height: '0%',
        width: '0%',
        visibility: 'visible',
    });
    test = 0;
});



$(document).ready(function() {
    var ccc = $('#menu-mobile ul li').find('ul');
    if (ccc.length !== 0) {
        ccc.before('<button class="accordion"></button>');
        ccc.addClass('sub-menu');
    }
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "200px";
            }
        }
    }
});