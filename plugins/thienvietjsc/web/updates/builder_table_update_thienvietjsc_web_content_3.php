<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateThienvietjscWebContent3 extends Migration
{
    public function up()
    {
        Schema::table('thienvietjsc_web_content', function($table)
        {
            $table->string('description3', 2000)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('thienvietjsc_web_content', function($table)
        {
            $table->dropColumn('description3');
        });
    }
}
