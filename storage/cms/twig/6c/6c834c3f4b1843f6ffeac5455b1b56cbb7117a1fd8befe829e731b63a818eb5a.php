<?php

/* /var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/pages/blog_detail.htm */
class __TwigTemplate_517ac91c00d0535920af3f2bacba2e1e58804448e46c4efc6c0071808dc52d19 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("blogPost"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 2
        echo "
<div class=\"wrap_section7\" id=\"section7\">
        <div class=\"container\">
            <div class=\"main_info\">
                <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">bài viết khác</h2>

                <div class=\"main_slide\">
                  ";
        // line 9
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("blogPosts"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 10
        echo "                </div> <!-- /main_slide -->
            </div> <!-- /main_info -->
        </div> <!-- /container -->
    </div> <!-- /wrap_section7 -->";
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/pages/blog_detail.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 10,  36 => 9,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% component 'blogPost' %}

<div class=\"wrap_section7\" id=\"section7\">
        <div class=\"container\">
            <div class=\"main_info\">
                <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">bài viết khác</h2>

                <div class=\"main_slide\">
                  {% component 'blogPosts' %}
                </div> <!-- /main_slide -->
            </div> <!-- /main_info -->
        </div> <!-- /container -->
    </div> <!-- /wrap_section7 -->", "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/pages/blog_detail.htm", "");
    }
}
