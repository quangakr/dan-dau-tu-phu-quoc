<?php

/* /var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/pages/home.htm */
class __TwigTemplate_091b72aa4985c2ad56d201867fa49614d850cbf6891dde98652fa7a6d514f8e4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"content\">


  <div class=\"wrap_section5\" id=\"section5\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">video dự án</h2>
      </div> <!-- /main_info -->

        <div class=\"row\">
          <div class=\"col-lg-10 offset-lg-1\">
            <div class=\"video_list\">
              <div class=\"row\">
                <div class=\"col-lg-6 col-md-6\">
                  <div class=\"video wow fadeInLeft\" data-wow-delay=\"0.8s\" data-wow-duration=\"1s\">
                    <div class=\"top\">
                      <a data-fancybox data-options='{\"src\": \"#exampleModal\", \"touch\": false, \"smallBtn\" : false}' href=\"javascript:;\">
                        <img src=\"images/img_video1.png\" alt=\"\" class=\"img-fluid\">

                        <span class=\"click_play\"><i class=\"fa fa-play\" aria-hidden=\"true\"></i></span>
                      </a>
                    </div> <!-- /top -->
                    <h4>Chung cư Udic Westlake Tây Hồ</h4>

                    <div style=\"display: none;\" id=\"exampleModal\">
                      <iframe width=\"600\" height=\"400\" src=\"https://www.youtube.com/embed/W_eb3szwCjc\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>
                    </div>
                  </div> <!-- /video -->
                </div> <!-- /col -->

                <div class=\"col-lg-6 col-md-6\">
                  <div class=\"video wow fadeInRight\" data-wow-delay=\"0.8s\" data-wow-duration=\"1s\">
                    <div class=\"top\">
                      <a data-fancybox data-options='{\"src\": \"#exampleModal2\", \"touch\": false, \"smallBtn\" : false}' href=\"javascript:;\">
                        <img src=\"images/img_video2.png\" alt=\"\" class=\"img-fluid\">

                        <span class=\"click_play\"><i class=\"fa fa-play\" aria-hidden=\"true\"></i></span>
                      </a>
                    </div> <!-- /top -->
                    <h4>review công trinh udic westlake</h4>

                    <div style=\"display: none;\" id=\"exampleModal2\">
                      <iframe width=\"600\" height=\"400\" src=\"https://www.youtube.com/embed/Sj7wN7wqhHw\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>
                    </div>
                  </div> <!-- /video -->
                </div> <!-- /col -->
              </div> <!-- /row -->
            </div> <!-- /video -->
          </div> <!-- /col -->
        </div> <!-- /row -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section5 -->


  <div class=\"wrap_section1\" id=\"section1\">
    <div class=\"container\">
      <div class=\"main_info\">
        <div class=\"row\">
          <div class=\"col-lg-5\">
            <div class=\"index_info wow fadeInLeft\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
              <h2>tổng quan về udic westlake tây hồ</h2>
              <p>
                <strong>1. Tên Dự án:</strong> Chung cư UDIC Westlake Tây Hồ <BR>
                <strong>2. Quy mô dự án:</strong> 3 Tòa Tháp cao 17,20,23 tầng nổi và 2 tầng hầm. Tòa Tháp A – Tòa Tháp B – Tòa Tháp C <BR>
                <strong>3. Chủ đầu tư:</strong> Tổng Công Ty Đầu Tư Phát Triển Hạ Tầng Đô Thị UDIC<BR>
                <strong>4. Đơn vị thi công:</strong> Tổng Công Ty Đầu Tư Phát Triển Hạ Tầng Đô Thị UDIC<BR>
                <strong>5. Đơn vị tư vấn thiết kế:</strong> Công Ty CP Tư Vấn Kiến Trúc Đô Thị Hà Nội – UAC<BR>
                <strong>6. Đơn vị tư vấn giám sát:</strong> Công Ty Cổ Phần TEXO Tư Vấn & Đầu Tư <BR>
                <strong>7. Vị trí:</strong> Lô CT 04 KĐT Nam Thăng Long ( CIPUTRA) Tây Hồ – Hà Nội. <BR>
                <strong>8. Tổng diện tích:</strong> 29.616m2 <BR>
                <strong>9. Tầng cao công trình:</strong> 17, 20 ,23  Tầng Căn Hộ; 02 Tầng Hầm. <BR>
                <strong>10. Diện tích căn hộ đa dạng:</strong> 2PN & 3PN, 4PN và DUPLEX <BR>
              </p>
            </div> <!-- /index_info -->
          </div> <!-- /col -->
          <div class=\"col-lg-7\">
            <div class=\"images_info wow fadeInRight\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
              <div class=\"images\">
                <img src=\"images/product1.png\" alt=\"\" class=\"img-fluid\">
              </div>
              <div class=\"title_img\">
                <h3>phối cảnh tổng thể</h3>
              </div>
            </div> <!-- /images_info -->
          </div> <!-- /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section1 -->

  <div class=\"wrap_section2\" id=\"section2\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">udic westlake tây hồ - vị trí đắc địa</h2>

        <div class=\"row\">
          <div class=\"col-lg-9\">
            <div class=\"info_iamges wow zoomIn\" data-wow-delay=\"0.8s\" data-wow-duration=\"1.2s\">
              <img src=\"images/map1.png\" alt=\"\" class=\"img-fluid\">
            </div>
          </div> <!-- /col -->
          <div class=\"col-lg-3\">
            <div class=\"index_info wow fadeInRight\" data-wow-delay=\"1s\" data-wow-duration=\"1.2s\">
              <h4>vị trí</h4>
              <p>
                UDIC WESTLAKE toạ lạc trên mặt đường Võ Chí Công gần cầu Nhật Tân - con đường cửa ngõ của thủ đô với 10 làn xe nên rất thuận tiện cho việc đi lại tới các trung tâm khác của thành phố Hà Nội
              </p>
              <h4>kết nối</h4>
              <p>
                - Cách Hồ Tây, Công viên nước Hồ Tây: 5 phút <br>
                - Cách sân tập Golf Ciputra: 10 phút <br>
                - Gần các trường học: Hà Nội Academy, trường quốc tế LHQ UNIS, trường quốc tế Singgapore SIS, trường đại học Nội Vụ, . . . <br>
                - Cách sân bay Nội Bài: 15 phút <br>
                - Cách bệnh viện tim Hà Nội: 5 phút <br>
              </p>
            </div>
          </div>
        </div> <!-- /row -->
      </div> <!-- /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section2 -->

  <div class=\"wrap_section3\" id=\"section3\">
    <div class=\"container\">
      <div class=\"main_info_top\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">thiết kế căn hộ loại 2 và 3 phòng ngủ</h2>
        <div class=\"row\">
          <div class=\"col-lg-6\">
            <div class=\"images wow fadeInLeft\" data-wow-delay=\"0.8s\" data-wow-duration=\"1s\">
              <img src=\"images/product2.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /images -->
          </div> <!-- /col -->
          <div class=\"col-lg-6\">
            <div class=\"images  wow fadeInRight\" data-wow-delay=\"0.8s\" data-wow-duration=\"1s\">
              <img src=\"images/product3.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /images -->
          </div> <!-- /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info_top -->

      <div class=\"main_info_mid\">
        <div class=\"row\">
          <div class=\"col-lg-10 offset-lg-2\">
            <div class=\"info wow flipInX\"  data-wow-delay=\"1s\" data-wow-duration=\"1.2s\">
              <h3>thiết kế vượt trội từ đơn vị hàng đầu Việt Nam</h3>
              <h6>tổng công ty đầu tư phát triển hạ tầng đô thị</h6>
              <p>
                UDIC Westlake là dự án của Tổng Công ty Đầu tư Phát triển Hạ tầng đô thị – UDIC dược khởi công xây dựng vào cuối năm 2016. UDIC là chủ đầu tư bất động sản uy tín hàng đầu tại Việt Nam, là chủ đầu tư của nhiều dự án uy tín và chất lượng khu đô thị Nam Thăng Long, UDIC Riverside... tại Hà Nội và nhiều dự án khác tại Hà Nội
              </p>
              <p>
                Với ý tưởng tạo dựng “Cuộc sống xanh”, dự án UDIC Westlake Tây Hồ vô cùng thân thiện với thiên nhiên và con người.
              </p>
              <p>
                Với mục tiêu là những khách hàng mong muốn một cuộc sống đẳng cấp tiện nghi và lượng lớn cư dân người nước ngoài như: Hàn Quốc, Nhật Bản, Trung Quốc… sống và làm việc tại Việt Nam, Udic Westlake Tây Hồ sẽ là một thành phố quốc tế thu nhỏ với văn hóa đa màu sắc.
              </p>
            </div> <!-- /info -->
          </div><!--  /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info_mid -->

      <div class=\"main_info_bot\">
        <div class=\"row\">
          <div class=\"col-lg-9 offset-lg-3\">
            <div class=\"row\">
              <div class=\"col-lg-4\">
                <div class=\"index_info\">
                  <h3 class=\"wow fadeInLeft\" data-wow-delay=\"0.7s\" data-wow-duration=\"1.5s\">thiết kế căn hộ loại duplex 4 phòng ngủ</h3>
                  <p class=\"wow fadeInUp\" data-wow-delay=\"0.8s\" data-wow-duration=\"1.5s\">
                    cảm nhận thiết kế tối ưu từ <strong>UDIC</strong>
                  </p>
                </div> <!-- /index_info -->
              </div> <!-- /col -->
              <div class=\"col-lg-8\">
                <div class=\"images wow jackInTheBox\" data-wow-delay=\"0.8s\" data-wow-duration=\"1.5s\">
                  <img src=\"images/product4.png\" alt=\"\" class=\"img-fluid\">
                </div>
              </div>
            </div> <!-- /row -->
          </div> <!-- /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info_bot -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section3 -->

  <div class=\"wrap_section4\" id=\"section4\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">những tiện ích dành cho dân cư</h2>

        <div class=\"main_album\">
          <div class=\"list_img\">
            <a class=\"wow fadeIn\" data-wow-delay=\"0.2s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product5.png\">
              <img src=\"images/product5.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product6.png\">
              <img src=\"images/product6.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.8s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product7.png\">
              <img src=\"images/product7.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"1s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product8.png\">
              <img src=\"images/product8.png\" alt=\"\" class=\"img-fluid\">
            </a>
          </div> <!-- /list_img -->
          <div class=\"list_img\">
            <a class=\"wow fadeIn\" data-wow-delay=\"1s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product9.png\">
              <img src=\"images/product9.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.8s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product10.png\">
              <img src=\"images/product10.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product11.png\">
              <img src=\"images/product11.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.2s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product12.png\">
              <img src=\"images/product12.png\" alt=\"\" class=\"img-fluid\">
            </a>
          </div> <!-- /list_img -->
          <div class=\"list_img\">
            <a class=\"wow fadeIn\" data-wow-delay=\"0.2s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product13.png\">
              <img src=\"images/product13.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product14.png\">
              <img src=\"images/product14.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.8s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product15.png\">
              <img src=\"images/product15.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"1s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product16.png\">
              <img src=\"images/product16.png\" alt=\"\" class=\"img-fluid\">
            </a>
          </div> <!-- /list_img -->
        </div> <!-- /main_album -->
      </div> <!--  /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section4 -->


  <div class=\"wrap_section6\" id=\"section6\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">mặt bằng căn hộ</h2>

        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"product_img wow zoomIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"1s\">
              <img src=\"images/map2.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /product_img -->
          </div> <!-- /col -->
          <div class=\"col-lg-6\">
            <div class=\"product_img wow zoomIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.3s\">
              <img src=\"images/map3.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /product_img -->
          </div> <!-- /col -->
          <div class=\"col-lg-6\">
            <div class=\"product_img wow zoomIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.3s\">
              <img src=\"images/map4.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /product_img -->
          </div> <!-- /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section6 -->

  <div class=\"wrap_section7\" id=\"section7\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">tin tức sự kiện</h2>

        <div class=\"main_slide\">
          ";
        // line 271
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("blogPosts"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 272
        echo "        </div> <!-- /main_slide -->
      </div> <!-- /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section7 -->

</div>
    ";
        // line 278
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("modal"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
    }

    public function getTemplateName()
    {
        return "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  307 => 278,  299 => 272,  295 => 271,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"content\">


  <div class=\"wrap_section5\" id=\"section5\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">video dự án</h2>
      </div> <!-- /main_info -->

        <div class=\"row\">
          <div class=\"col-lg-10 offset-lg-1\">
            <div class=\"video_list\">
              <div class=\"row\">
                <div class=\"col-lg-6 col-md-6\">
                  <div class=\"video wow fadeInLeft\" data-wow-delay=\"0.8s\" data-wow-duration=\"1s\">
                    <div class=\"top\">
                      <a data-fancybox data-options='{\"src\": \"#exampleModal\", \"touch\": false, \"smallBtn\" : false}' href=\"javascript:;\">
                        <img src=\"images/img_video1.png\" alt=\"\" class=\"img-fluid\">

                        <span class=\"click_play\"><i class=\"fa fa-play\" aria-hidden=\"true\"></i></span>
                      </a>
                    </div> <!-- /top -->
                    <h4>Chung cư Udic Westlake Tây Hồ</h4>

                    <div style=\"display: none;\" id=\"exampleModal\">
                      <iframe width=\"600\" height=\"400\" src=\"https://www.youtube.com/embed/W_eb3szwCjc\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>
                    </div>
                  </div> <!-- /video -->
                </div> <!-- /col -->

                <div class=\"col-lg-6 col-md-6\">
                  <div class=\"video wow fadeInRight\" data-wow-delay=\"0.8s\" data-wow-duration=\"1s\">
                    <div class=\"top\">
                      <a data-fancybox data-options='{\"src\": \"#exampleModal2\", \"touch\": false, \"smallBtn\" : false}' href=\"javascript:;\">
                        <img src=\"images/img_video2.png\" alt=\"\" class=\"img-fluid\">

                        <span class=\"click_play\"><i class=\"fa fa-play\" aria-hidden=\"true\"></i></span>
                      </a>
                    </div> <!-- /top -->
                    <h4>review công trinh udic westlake</h4>

                    <div style=\"display: none;\" id=\"exampleModal2\">
                      <iframe width=\"600\" height=\"400\" src=\"https://www.youtube.com/embed/Sj7wN7wqhHw\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>
                    </div>
                  </div> <!-- /video -->
                </div> <!-- /col -->
              </div> <!-- /row -->
            </div> <!-- /video -->
          </div> <!-- /col -->
        </div> <!-- /row -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section5 -->


  <div class=\"wrap_section1\" id=\"section1\">
    <div class=\"container\">
      <div class=\"main_info\">
        <div class=\"row\">
          <div class=\"col-lg-5\">
            <div class=\"index_info wow fadeInLeft\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
              <h2>tổng quan về udic westlake tây hồ</h2>
              <p>
                <strong>1. Tên Dự án:</strong> Chung cư UDIC Westlake Tây Hồ <BR>
                <strong>2. Quy mô dự án:</strong> 3 Tòa Tháp cao 17,20,23 tầng nổi và 2 tầng hầm. Tòa Tháp A – Tòa Tháp B – Tòa Tháp C <BR>
                <strong>3. Chủ đầu tư:</strong> Tổng Công Ty Đầu Tư Phát Triển Hạ Tầng Đô Thị UDIC<BR>
                <strong>4. Đơn vị thi công:</strong> Tổng Công Ty Đầu Tư Phát Triển Hạ Tầng Đô Thị UDIC<BR>
                <strong>5. Đơn vị tư vấn thiết kế:</strong> Công Ty CP Tư Vấn Kiến Trúc Đô Thị Hà Nội – UAC<BR>
                <strong>6. Đơn vị tư vấn giám sát:</strong> Công Ty Cổ Phần TEXO Tư Vấn & Đầu Tư <BR>
                <strong>7. Vị trí:</strong> Lô CT 04 KĐT Nam Thăng Long ( CIPUTRA) Tây Hồ – Hà Nội. <BR>
                <strong>8. Tổng diện tích:</strong> 29.616m2 <BR>
                <strong>9. Tầng cao công trình:</strong> 17, 20 ,23  Tầng Căn Hộ; 02 Tầng Hầm. <BR>
                <strong>10. Diện tích căn hộ đa dạng:</strong> 2PN & 3PN, 4PN và DUPLEX <BR>
              </p>
            </div> <!-- /index_info -->
          </div> <!-- /col -->
          <div class=\"col-lg-7\">
            <div class=\"images_info wow fadeInRight\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.2s\">
              <div class=\"images\">
                <img src=\"images/product1.png\" alt=\"\" class=\"img-fluid\">
              </div>
              <div class=\"title_img\">
                <h3>phối cảnh tổng thể</h3>
              </div>
            </div> <!-- /images_info -->
          </div> <!-- /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section1 -->

  <div class=\"wrap_section2\" id=\"section2\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">udic westlake tây hồ - vị trí đắc địa</h2>

        <div class=\"row\">
          <div class=\"col-lg-9\">
            <div class=\"info_iamges wow zoomIn\" data-wow-delay=\"0.8s\" data-wow-duration=\"1.2s\">
              <img src=\"images/map1.png\" alt=\"\" class=\"img-fluid\">
            </div>
          </div> <!-- /col -->
          <div class=\"col-lg-3\">
            <div class=\"index_info wow fadeInRight\" data-wow-delay=\"1s\" data-wow-duration=\"1.2s\">
              <h4>vị trí</h4>
              <p>
                UDIC WESTLAKE toạ lạc trên mặt đường Võ Chí Công gần cầu Nhật Tân - con đường cửa ngõ của thủ đô với 10 làn xe nên rất thuận tiện cho việc đi lại tới các trung tâm khác của thành phố Hà Nội
              </p>
              <h4>kết nối</h4>
              <p>
                - Cách Hồ Tây, Công viên nước Hồ Tây: 5 phút <br>
                - Cách sân tập Golf Ciputra: 10 phút <br>
                - Gần các trường học: Hà Nội Academy, trường quốc tế LHQ UNIS, trường quốc tế Singgapore SIS, trường đại học Nội Vụ, . . . <br>
                - Cách sân bay Nội Bài: 15 phút <br>
                - Cách bệnh viện tim Hà Nội: 5 phút <br>
              </p>
            </div>
          </div>
        </div> <!-- /row -->
      </div> <!-- /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section2 -->

  <div class=\"wrap_section3\" id=\"section3\">
    <div class=\"container\">
      <div class=\"main_info_top\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">thiết kế căn hộ loại 2 và 3 phòng ngủ</h2>
        <div class=\"row\">
          <div class=\"col-lg-6\">
            <div class=\"images wow fadeInLeft\" data-wow-delay=\"0.8s\" data-wow-duration=\"1s\">
              <img src=\"images/product2.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /images -->
          </div> <!-- /col -->
          <div class=\"col-lg-6\">
            <div class=\"images  wow fadeInRight\" data-wow-delay=\"0.8s\" data-wow-duration=\"1s\">
              <img src=\"images/product3.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /images -->
          </div> <!-- /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info_top -->

      <div class=\"main_info_mid\">
        <div class=\"row\">
          <div class=\"col-lg-10 offset-lg-2\">
            <div class=\"info wow flipInX\"  data-wow-delay=\"1s\" data-wow-duration=\"1.2s\">
              <h3>thiết kế vượt trội từ đơn vị hàng đầu Việt Nam</h3>
              <h6>tổng công ty đầu tư phát triển hạ tầng đô thị</h6>
              <p>
                UDIC Westlake là dự án của Tổng Công ty Đầu tư Phát triển Hạ tầng đô thị – UDIC dược khởi công xây dựng vào cuối năm 2016. UDIC là chủ đầu tư bất động sản uy tín hàng đầu tại Việt Nam, là chủ đầu tư của nhiều dự án uy tín và chất lượng khu đô thị Nam Thăng Long, UDIC Riverside... tại Hà Nội và nhiều dự án khác tại Hà Nội
              </p>
              <p>
                Với ý tưởng tạo dựng “Cuộc sống xanh”, dự án UDIC Westlake Tây Hồ vô cùng thân thiện với thiên nhiên và con người.
              </p>
              <p>
                Với mục tiêu là những khách hàng mong muốn một cuộc sống đẳng cấp tiện nghi và lượng lớn cư dân người nước ngoài như: Hàn Quốc, Nhật Bản, Trung Quốc… sống và làm việc tại Việt Nam, Udic Westlake Tây Hồ sẽ là một thành phố quốc tế thu nhỏ với văn hóa đa màu sắc.
              </p>
            </div> <!-- /info -->
          </div><!--  /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info_mid -->

      <div class=\"main_info_bot\">
        <div class=\"row\">
          <div class=\"col-lg-9 offset-lg-3\">
            <div class=\"row\">
              <div class=\"col-lg-4\">
                <div class=\"index_info\">
                  <h3 class=\"wow fadeInLeft\" data-wow-delay=\"0.7s\" data-wow-duration=\"1.5s\">thiết kế căn hộ loại duplex 4 phòng ngủ</h3>
                  <p class=\"wow fadeInUp\" data-wow-delay=\"0.8s\" data-wow-duration=\"1.5s\">
                    cảm nhận thiết kế tối ưu từ <strong>UDIC</strong>
                  </p>
                </div> <!-- /index_info -->
              </div> <!-- /col -->
              <div class=\"col-lg-8\">
                <div class=\"images wow jackInTheBox\" data-wow-delay=\"0.8s\" data-wow-duration=\"1.5s\">
                  <img src=\"images/product4.png\" alt=\"\" class=\"img-fluid\">
                </div>
              </div>
            </div> <!-- /row -->
          </div> <!-- /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info_bot -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section3 -->

  <div class=\"wrap_section4\" id=\"section4\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">những tiện ích dành cho dân cư</h2>

        <div class=\"main_album\">
          <div class=\"list_img\">
            <a class=\"wow fadeIn\" data-wow-delay=\"0.2s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product5.png\">
              <img src=\"images/product5.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product6.png\">
              <img src=\"images/product6.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.8s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product7.png\">
              <img src=\"images/product7.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"1s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product8.png\">
              <img src=\"images/product8.png\" alt=\"\" class=\"img-fluid\">
            </a>
          </div> <!-- /list_img -->
          <div class=\"list_img\">
            <a class=\"wow fadeIn\" data-wow-delay=\"1s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product9.png\">
              <img src=\"images/product9.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.8s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product10.png\">
              <img src=\"images/product10.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product11.png\">
              <img src=\"images/product11.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.2s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product12.png\">
              <img src=\"images/product12.png\" alt=\"\" class=\"img-fluid\">
            </a>
          </div> <!-- /list_img -->
          <div class=\"list_img\">
            <a class=\"wow fadeIn\" data-wow-delay=\"0.2s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product13.png\">
              <img src=\"images/product13.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product14.png\">
              <img src=\"images/product14.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"0.8s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product15.png\">
              <img src=\"images/product15.png\" alt=\"\" class=\"img-fluid\">
            </a>
            <a class=\"wow fadeIn\" data-wow-delay=\"1s\" data-wow-duration=\"2s\" data-fancybox=\"images\" href=\"images/product16.png\">
              <img src=\"images/product16.png\" alt=\"\" class=\"img-fluid\">
            </a>
          </div> <!-- /list_img -->
        </div> <!-- /main_album -->
      </div> <!--  /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section4 -->


  <div class=\"wrap_section6\" id=\"section6\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">mặt bằng căn hộ</h2>

        <div class=\"row\">
          <div class=\"col-lg-12\">
            <div class=\"product_img wow zoomIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"1s\">
              <img src=\"images/map2.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /product_img -->
          </div> <!-- /col -->
          <div class=\"col-lg-6\">
            <div class=\"product_img wow zoomIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.3s\">
              <img src=\"images/map3.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /product_img -->
          </div> <!-- /col -->
          <div class=\"col-lg-6\">
            <div class=\"product_img wow zoomIn\" data-wow-delay=\"0.5s\" data-wow-duration=\"1.3s\">
              <img src=\"images/map4.png\" alt=\"\" class=\"img-fluid\">
            </div> <!-- /product_img -->
          </div> <!-- /col -->
        </div> <!-- /row -->
      </div> <!-- /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section6 -->

  <div class=\"wrap_section7\" id=\"section7\">
    <div class=\"container\">
      <div class=\"main_info\">
        <h2 class=\"wow fadeInUp\" data-wow-delay=\"0.5s\">tin tức sự kiện</h2>

        <div class=\"main_slide\">
          {% component 'blogPosts' %}
        </div> <!-- /main_slide -->
      </div> <!-- /main_info -->
    </div> <!-- /container -->
  </div> <!-- /wrap_section7 -->

</div>
    {% partial 'modal' %}", "/var/www/vhosts/udictayho.com/httpdocs/themes/udic-tay-ho/pages/home.htm", "");
    }
}
