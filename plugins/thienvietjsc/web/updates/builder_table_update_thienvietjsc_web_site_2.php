<?php namespace Thienvietjsc\Web\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateThienvietjscWebSite2 extends Migration
{
    public function up()
    {
        Schema::table('thienvietjsc_web_site', function($table)
        {
            $table->string('logo', 500)->nullable();
            $table->string('favicon', 500)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('thienvietjsc_web_site', function($table)
        {
            $table->dropColumn('logo');
            $table->dropColumn('favicon');
        });
    }
}
